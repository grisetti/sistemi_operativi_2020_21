#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"

void internal_revertAndPreempt()
{
  if (running)
  {
    disastrOS_debug("PREEMPT - %d ->", running->pid);
  }

  if (!ready_list.size || !ready_list.first)
  {
    running->syscall_retvalue = DSOS_EREVERT_AND_PREEMPT;
    return;
  }

  ListItem *current = ready_list.first;
  while (current)
  {
    ListItem *tmp = current->next;
    current->next = current->prev;
    current->prev = tmp;
    current = tmp;
  }
  ListItem *tmp = ready_list.first;
  ready_list.first = ready_list.last;
  ready_list.last = tmp;

  if (ready_list.first)
  {
    PCB *next_process = (PCB *)List_detach(&ready_list, ready_list.first);
    running->status = Ready;
    List_insert(&ready_list, ready_list.last, (ListItem *)running);
    next_process->status = Running;
    running = next_process;
  }

  if (running)
  {
    disastrOS_debug(" %d\n", running->pid);
  }
  running->syscall_retvalue = 0;
}