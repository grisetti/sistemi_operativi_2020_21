#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"
#include "float_linked_list.h"

#define TARGET_LIST_SIZE 10

int main(int argc, char** argv) {
  printf("float_linked_list|start\n");

  //ia simple float list
  ListHead float_list;
  List_init(&float_list);
  for (int i = 0; i < TARGET_LIST_SIZE; ++i) {
    FloatListItem* f_item = (FloatListItem*)(malloc(sizeof(FloatListItem)));
    ListItem_init((ListItem*) f_item);
    f_item->value = i + 0.5f;
    assert(f_item && "ERROR, cannot instantiate item");
    List_insert(&float_list, float_list.last, (ListItem*)f_item);
  }
  FloatList_print(&float_list);
  
  //ia list of lists (float)
  ListHead float_matrix;
  List_init(&float_matrix);
  FloatListList_init(&float_matrix, 3, 3);

  FloatListList_at(&float_matrix, 0 , 0)->value = 5.0;
  FloatListList_at(&float_matrix, 0 , 2)->value = 2.0;
  FloatListList_at(&float_matrix, 1 , 1)->value = 3.0;
  FloatListList_at(&float_matrix, 1 , 0)->value = 2.6;
  FloatListList_at(&float_matrix, 2 , 0)->value = 4.0;
  FloatListList_at(&float_matrix, 2 , 2)->value = 0.6;
  FloatListList_print(&float_matrix);

  //ia sum rows
  ListHead sum_list;
  List_init(&sum_list);
  FloatListList_sumRows(&sum_list, &float_matrix);
  FloatList_print(&sum_list);

  //ia checkout
  FloatList_destroy(&float_list);
  FloatListList_destroy(&float_matrix);
  FloatList_destroy(&sum_list);

  printf("float_linked_list|end\n");
  return 0;
}
